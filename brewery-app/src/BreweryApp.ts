/* eslint-disable no-undef */
import {
  LitElement,
  html,
  css,
  PropertyDeclarations
} from 'lit';
import { property } from 'lit/decorators.js';

export class BreweryApp extends LitElement {
  @property({ type: Array })
  private breweries: Brewery[] = [];

  @property({ type: Boolean, state: true })
  private loading: boolean = false;

  static get properties(): PropertyDeclarations {
    return {
      breweries: { type: Array },
      title: { type: String },
    };
  }

  connectedCallback(): void {
    super.connectedCallback();
    if (!this.breweries.length) this.fetchBreweries();
  }

  async fetchBreweries(): Promise<void> {
    this.loading = true;
    const response = await fetch(
      'https://api.openbrewerydb.org/breweries?by_city=minneapolis'
    );
    const jsonResp = await response.json();
    this.breweries = jsonResp;
    this.loading = false;
  }

  render() {
    const totalVistied = this.breweries.filter(b => b.visited).length
    const totalUnvisited = this.breweries.length - totalVistied

    return html`
      <h1>Kewl Brews</h1>
      <h2>Breweries App</h2>
      <p>Total breweries: ${this.breweries.length}</p>
      <p>Total visited: ${totalVistied}, remaining: ${totalUnvisited}</p>

      ${this.loading ? html`<p>Loading...</p>` : ''}
      <ul>
        ${this.breweries.map(
          brewery => html`
            <li item-key="${brewery.id}">
              ${brewery.name}
              <button
                @click="${() => this.handleToggleVisitedStatus(brewery)}"
                style="
                  color: #353535;
                  background-color: ${brewery.visited
                  ? 'lightgreen'
                  : 'lightgray'};
                  border: 0;
                  box-shadow: 2px 2px 5px ${brewery.visited
                  ? 'lightgreen'
                  : 'lightgray'};
                  padding: .25rem .5rem;
                "
              >
                ${brewery.visited ? 'mark un-visited' : 'mark visited'}
              </button>
            </li>
          `
        )}
      </ul>
    `;
  }

  private handleToggleVisitedStatus(brewToUpdate: Brewery): void {
    this.breweries = this.breweries.map(brewery =>
      brewery === brewToUpdate
        ? { ...brewery, visited: !brewery.visited }
        : brewery
    );
  }
}
